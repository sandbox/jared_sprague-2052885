<?php

function memcache_enhanced_drush_command() {
  $items = array();

  $items['cache-stats'] = array(
    'description' => "Get stats about the current cache.",
    'options' => array(
      'format' => 'print_r (default), json',
    ),
    'examples' => array(
      'drush cache-stats' => 'Gets stats for the whole cache.',
    ),
    'aliases' => array('cst'),
      'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  return $items;
}

function drush_memcache_enhanced_cache_stats() {
  $stats_ag = memcache_enhanced_get_stats();

  $format = '';
  if (strtolower(drush_get_option('format')) == 'json') {
    echo json_encode($stats_ag) . "\n";
  } else { 
    print_r($stats_ag);
  }
}
