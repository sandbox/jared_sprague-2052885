## INSTALLATION ##
1. Edit settings.php and add the following lines:

     # Add this cache to the list of available cache implemntations
     $conf['cache_backends'][] = 'sites/all/modules/memcache_enhanced/memcache_enhanced.inc';

     # Add cache bins that you want to compress
     $conf['cache_default_class'] = 'MemcacheEnhancedCache';
