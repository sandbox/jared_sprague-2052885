<?php

/**
 *  Memcache wrapper that will fail over to the default drupal database cache if no memcache server can be reached.
 *
 */ 
class MemcacheEnhancedCache implements DrupalCacheInterface {
  private $_cacheBackend;
  private $_bin;

  /**
   * Constructs a new cache interface.
   *
   * @param $bin
   *   The cache bin for which the object is created.
   */
  function __construct($bin) {
    $this->_cacheBackend = new MemCacheDrupal($bin);
    $this->_bin = $bin;

    if ($this->_cacheBackend->memcache === FALSE) {
      $memcache_error_msg = "Failed to connect to memcache";        

      // remove the memcache "Failed to connect" error message
      if (isset($_SESSION['messages']['error'])) {
        $num_error_msgs = count($_SESSION['messages']['error']);

        if ($num_error_msgs > 1) { 
          // search through the error messages for any memcache failed message and remove it
          for ($i = 0; $i < count($_SESSION['messages']['error']); $i++) { 
            if (strstr($_SESSION['messages']['error'][$i], $memcache_error_msg)) {
              unset($_SESSION['messages']['error'][$i]);
            } 
          }
        } else if (strstr($_SESSION['messages']['error'][0], $memcache_error_msg)) {
          // the memcache error message is the only error message in the queue, so unset all errors so the error message box is removed
          unset($_SESSION['messages']['error']);
        }
      }

      // log that memcache server is down
      watchdog('memcache_enhanced', t("Failed to connect to memcache server. Failing over to database cache."), array(), WATCHDOG_ERROR);
    
      // failover to default Drupal database cache
      $this->_cacheBackend = new DrupalDatabaseCache($bin);
    } 
  }

  /**
   * Returns data from the persistent cache.
   *
   * Data may be stored as either plain text or as serialized data. cache_get()
   * will automatically return unserialized objects and arrays.
   *
   * @param $cid
   *   The cache ID of the data to retrieve.
   *
   * @return
   *   The cache or FALSE on failure.
   */
  function get($cid) {
    return $this->_cacheBackend->get($cid);
  }

  /**
   * Returns data from the persistent cache when given an array of cache IDs.
   *
   * @param $cids
   *   An array of cache IDs for the data to retrieve. This is passed by
   *   reference, and will have the IDs successfully returned from cache
   *   removed.
   *
   * @return
   *   An array of the items successfully returned from cache indexed by cid.
   */
   function getMultiple(&$cids) {
     return $this->_cacheBackend->getMultiple($cids);
   }

  /**
   * Stores data in the persistent cache.
   *
   * @param $cid
   *   The cache ID of the data to store.
   * @param $data
   *   The data to store in the cache. Complex data types will be automatically
   *   serialized before insertion.
   *   Strings will be stored as plain text and not serialized.
   * @param $expire
   *   One of the following values:
   *   - CACHE_PERMANENT: Indicates that the item should never be removed unless
   *     explicitly told to using cache_clear_all() with a cache ID.
   *   - CACHE_TEMPORARY: Indicates that the item should be removed at the next
   *     general cache wipe.
   *   - A Unix timestamp: Indicates that the item should be kept at least until
   *     the given time, after which it behaves like CACHE_TEMPORARY.
   */
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    // save information about this set
    if (!empty($cid) && !empty($this->_bin)) {
      $cacheBackend = NULL;

      if (get_class($this->_cacheBackend) == 'MemCacheDrupal') {
        $cacheBackend = new MemCacheDrupal('cache');
      } else {
        $cacheBackend = new DrupalDatabaseCache('cache');
      }

      $stats = $cacheBackend->get('memcache_enhanced_stats')->data;
      $item_data = array('size' => $this->_get_serialized_size($data), 'expire' => $expire);
 
      if (!is_array($stats)) {
        $stats = array($this->_bin => array($cid => $item_data));
      } else if (!is_array($stats[$this->_bin])) {
        $stats[$this->_bin] = array($cid => $item_data);
      } else {
        $stats[$this->_bin][$cid] = $item_data;
      }
      
      $cacheBackend->set('memcache_enhanced_stats', $stats, CACHE_PERMANENT);
    } 

    $this->_cacheBackend->set($cid, $data, $expire);
  }

  /**
   *  calculates the byte size of the serialized data
   */
  private function _get_serialized_size($obj) {
    $serialized_obj = serialize($obj);
    if (function_exists('mb_strlen')) {
      $size = mb_strlen($serialized_obj, '8bit');
    } else {
      $size = strlen($serialized_obj);
    }  

    return $size;
  }

  /**
   * Expires data from the cache.
   *
   * If called without arguments, expirable entries will be cleared from the
   * cache_page and cache_block bins.
   *
   * @param $cid
   *   If set, the cache ID to delete. Otherwise, all cache entries that can
   *   expire are deleted.
   * @param $wildcard
   *   If set to TRUE, the $cid is treated as a substring
   *   to match rather than a complete ID. The match is a right hand
   *   match. If '*' is given as $cid, the bin $bin will be emptied.
   */
  function clear($cid = NULL, $wildcard = FALSE) {
    $this->_cacheBackend->clear($cid, $wildcard);
  }

  /**
   * Checks if a cache bin is empty.
   *
   * A cache bin is considered empty if it does not contain any valid data for
   * any cache ID.
   *
   * @return
   *   TRUE if the cache bin specified is empty.
   */
  function isEmpty() {
    return $this->_cacheBackend->isEmpty();
  }
}
 
/**
 * Logs to /tmp/rh_drupal_debug.txt
 * overwrites the file after each call
 */
function memcache_enhanced_loggit($item, $mode = 'w') {
  $sep = "#########################################\n";
  $new_lines = "\n\n\n\n";
  $fp = fopen('/tmp/drupal_debug.txt', $mode);
  fwrite($fp, $sep . print_r($item, TRUE) . $new_lines);
  fclose($fp);
}

